import React from "react";
import "./App.css";
class App extends React.Component {
    // Constructor
    constructor(props) {
        super(props);
 
        this.state = {
            items: [],
            DataisLoaded: false,
        };
    }
 
    // ComponentDidMount is used to
    // execute the code
    componentDidMount() {
        fetch("http://localhost:52866/weatherforecast")
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    items: json,
                    DataisLoaded: true,
                });
            });
            console.log(this.state);
    }
    render() {
        const { DataisLoaded, items } = this.state;
        if (!DataisLoaded)
            return (
                <div>
                    <h1> Pleses wait some time.... </h1>
                </div>
            );
 
        return (
            <div className="App" >
                <h1 className="geeks">Hello!) I say you about weather</h1>
                <h3>I fetch weather for you</h3>
                <table border="1" cellspacing="1" cellpadding="1">
                  <tr>
                    <td>Summary</td><td>Date</td><td>TemperatureC</td><td>temperatureF</td>
                  </tr>
                      {items.map((item) => (<tr><td>{item.summary}</td><td>{item.date}</td><td>{item.temperatureC}</td><td>{item.temperatureF}</td></tr>))}
                </table>  
            </div>
        );
    }
}
 
export default App;